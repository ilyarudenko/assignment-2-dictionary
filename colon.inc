%define START_INDEX 0
%macro colon 2
    %%next: dq START_INDEX
    db %1, 0
    %2:
%define START_INDEX %%next
%endmacro