%include "lib.inc"

section .text

global find_word

find_word:
    cmp rsi, 0
    je .fail
    add rsi, 8
    push rdi
    push rsi
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1
    sub rsi, 8
    jz .success
    mov rsi, [rsi]
    jmp find_word

    .fail:
        mov rax, 0
        ret

    .success:
        mov rax, rsi
        ret