%include "lib.inc"
%include "colon.inc"
%define size_of_buffer 256

section .rodata
%include 'words.inc'
enter_message: db "Enter key: ", 0
fail_message: db "Key except", 0
too_long_message: db "Key too long", 0
value_message: db "Got value: ", 0

section .bss
buffer: resb size_of_buffer

section .text

global _start
extern find_word

_start:
        mov rdi, enter_message
        call print_string
        mov rdi, buffer
        mov rsi, size_of_buffer
        call read_word
        cmp rax, 0
        je .too_long_issue
        push rdx
        mov rdi, buffer
        mov rsi, START_INDEX
        call find_word
        cmp rax, 0
        je .fail
        pop rdx
        add rax, rdx
        inc rax
        push rax
        je .success

    .success:
        mov rdi, value_message
        call print_string
        pop rdi
        call print_string
        call print_newline
        mov rdi, 0
        call exit

    .fail:
        mov rdi, fail_message
        call print_string
        mov rdi, 1
        call exit

    .too_long_issue:
        mov rdi, too_long_message
        call print_string
        mov rdi, 1
        call exit